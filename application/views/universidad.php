<br>
<h1>Hello Universidad</h1>
<br>
<img src="<?php echo base_url('assets/img/utc.png') ?>" alt="">
<div id='mapa1' style='width:100%;height:900px;'>

</div>

<script>
    const crearMarker = (coordenada, mapa, titulo) => {
        return new google.maps.Marker({
            position: coordenada,
            map: mapa,
            title: titulo
        })
    }

     function initMap() {
        const coordenadaCentral =   new google.maps.LatLng(-0.9178895200378961, -78.63298753905256)
        //TODO: Crear un mapa
        const mapa =   new google.maps.Map(
            document.getElementById('mapa1'), {
                center: coordenadaCentral,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        )

        //TODO: Crear un marcador
        const markerUtc = new google.maps.Marker({
            position: coordenadaCentral,
            map: mapa,
            title: 'Universidad Tecnica de Cotopaxi - La Matriz',
            icon: '<?php echo base_url('assets/img/utc.png') ?>'
        })

        const markSalache = new google.maps.Marker({
            position: new google.maps.LatLng(-0.9995333895606218, -78.6233761017107),
            map: mapa,
            title: 'Universidad Tecnica de Cotopaxi - Salache',
        })

        const markPujili = new google.maps.Marker({
            position: new google.maps.LatLng(-0.9582910148279896, -78.69647655010989),
            map: mapa,
            title: 'Universidad Tecnica de Cotopaxi - Pujili',
        })

        const markLamana = new google.maps.Marker({
            position: new google.maps.LatLng(-0.931979614066811, -79.24087279347962),
            map: mapa,
            title: 'Universidad Tecnica de Cotopaxi - Lamana',
        })
    }
</script>
